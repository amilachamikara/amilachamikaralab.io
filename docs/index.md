---
hide:
    - navigation
    - toc
---
# Welcome



Hello and a warm welcome to my personal website! I'm Amila Jayawardhana, a passionate and skilled software engineer with over five years of industry experience. Whether you're here to learn more about my professional background or simply browsing for inspiration, I hope you'll find this site informative and enjoyable.

As a technical enthusiast, I'm constantly seeking new challenges and opportunities to expand my knowledge and skills. I pride myself on my ability to design, develop, test, and deploy end-to-end enterprise-level applications that adhere to the highest industry standards. And as a self-motivated team player, I'm always willing to share my expertise, experiment with new approaches, and collaborate with others to achieve common goals.

Above all, I believe that a positive attitude and a flexible approach are essential for success in any environment. So whether you're a fellow developer, a prospective client, or simply curious about the world of software engineering, I invite you to explore my website and learn more about what I have to offer. Thanks for stopping by!
<br/><br/><br/>


![Signature](assets/signature.png#only-light){width="100", loading=lazy }![Signature](assets/signature(dark).png#only-dark){width="100", loading=lazy }  
Amila Jayawardhana
<span hidden>pout2q3y4RDC</span>
