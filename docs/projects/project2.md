2014

This is my second year project in university which is simply called as media organizing tool. 
We all are listening and watching media in our day-to-day life. People has different preferences among each others. 
We are listening to one type of medias in mornings and another type of medias at night. Also, if we are doing some coding, then we prefer another type of medias at that time. 
So those preferences are changed from one person to the other. So this media organizing tool's main objective is to suggest playlists to the user, according to the time, work that they are doing on the machine at that time. 
Also, this tool is going to propose playlist for the people who has same preferences among a network. 

Technologies used: Java ,C# ,MySql, Android for mobile application, 

Project Supervisor : Mr. Omega Silva (Associate Software Architect at Virtusa) 
Project Supervisor : Mr. Thilina thanthiriwatte (Lecturer of faculty of information technology,University of Moratuwa)