2011

NeoCash is an inventory control system for a retail shop. This was the final project of JSAD level when I was a student at Java Institute. This system was developed to cater following requirement of the customer.

* make printed invoices to buyers (using barcode scanners)
* generate barcodes to products
* store GRN details of purchases
* maintain the stock while notifying the stock levels (within the software or by SMS)
* generate reports (printed & digital)
* maintain payroll system
* maintain user details, employee details, and log reports

Additionally, this system has high security features. When someone tries to log into the system more than three times the system will automatically be locked and the unlock key will be sent to the administrator by a SMS. 