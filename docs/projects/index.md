# Projects

**[Natural Language Interface for Databases](project1.md)**<br/>
Research Project | University of Moratuwa, Level 4<br/>
`Python`, `OpenCV`, `NLTK`, `QT`, `MySQL`


**[Personal Media Organizing Tool](project2.md)**<br/>
Industry Based Software Project | University of Moratuwa, Level 2<br/>
`Java`, `C#`, `MySQL`, `PHP`, `Android`


**[Low Cost 3D Scanner](project3.md)**<br/>
Microcontroller Based Hardware Project | University of Moratuwa, Level 1<br/>
`PIC Microcontroller`, `Micro C`, `Java`, `IR Sensor`


**[Neo Cash](project4.md)**<br/>
An inventory control system for a retail shop | Java Institute - Final Project<br/>
[PIC Microcontroller, Micro C, Java, IR Sensor