2016

The aim of this project is to create a system which can overcome the barrier between
the man and the databases. The proposed solution consists of four parts. The for parts will
finally be combined into one system which will make the user interaction of databases more efficient
and user-friendly.

<figure markdown>
![High level design of the system](../assets/project1_figure1.png){ width=500 }
<figcaption>High level system design</figcaption>
</figure>

The First part will be to process a given scenario and create the initial database. This module
must decide on the key attributes, non-key attributes, table names and data types etc. This will
be done using certain set of constraints and using Natural Language Processing (NLP). The
Input will be a scenario and the users must give it to the system.

<figure markdown>
![Database creator](../assets/project1_figure2.png){ width=500 }
</figure>

The Second part of the system will the normalization of the database. For this the databases
scenario and the initial database is taken as an input. Then the system will Decide on the set of
functional dependencies using Natural Language Processing. Then using inference rules such
as Armstrong’s Axioms we can predict on the possible functional dependencies making it a
basis for normalization.

The third part will identify the extracted information of the given diagram and then the system
will be able to retrieve the information from the database as necessary. This part will have a
Natural Language Processing (NLP) part which will map the identified word images to the
exact word, and then we should be able to get the relevant query made from the words
processed. This part is used by the external users and the inputs will be the diagram drawn
using a camera. OpenCV and NLTK is used for the implementation.

<figure markdown>
![img.png](../assets/project1_figure3_1.png){ width=300 }
</figure>

<figure markdown>
![img.png](../assets/project1_figure3_2.png){ width=500 }
</figure>

The final part of the application will handle the natural language queries that will be converted
to SQL queries in the application. The user will be able to get query result from simple
questions in natural language. This part will be able to take normal questions and statements
and then convert them to database queries.

<figure markdown>
![Database querying system](../assets/project1_figure4.png){ width=500 }
</figure>

:material-github: Source Code: [https://github.com/amilachamikara/isyntax](https://github.com/amilachamikara/isyntax)