---
hide:
    navigation
---

:material-email: [chamikara.amila@gmail.com](mailto:chamikara.amila@gmail.com)

:material-github: [https://www.github.com/amilacjay](https://www.github.com/amilacjay){target=_blank}

:material-linkedin: [https://www.linkedin.com/in/amilacjay](https://www.linkedin.com/in/amilacjay){target=_blank}

:fontawesome-brands-researchgate: [https://www.researchgate.net/profile/Amila-Jayawardhana](https://www.researchgate.net/profile/Amila-Jayawardhana){target=_blank}


