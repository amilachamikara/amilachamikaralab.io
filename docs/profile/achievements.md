# :material-trophy-variant: Achievements

* First Class for Bachelor’s Degree, University of Moratuwa
* Dean's List Student in Semester 1 and Semester 5, University of Moratuwa
* 4th Place of Fuegosticka 2.0 Android Game Development Competition, University of Moratuwa
* Batch Top of Java Institute, Kandy (2012)