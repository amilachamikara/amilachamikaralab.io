# :material-star: Skills & Competencies

### Professional

* **AI Technologies:** LangChain, RAG applications, LLMs, Agents ★★★☆☆
* **Backend Technologies:** Java, Python Spring, JPA, Maven, Tomcat ★★★★☆
* **REST API:** OpenAPI3, OAuth2 ★★★☆☆
* **Frontend Technologies:** Angular, React ★★★☆☆
* **CI/CD:** GitLab Pipelines, Jenkins ★★★☆☆
* **Containers:** Docker ★★★☆☆

### Educational and Other

* Image Processing: OpenCV with Python ★★★☆☆
* Microcontroller Programming: MicroC ★★★☆☆
* Compilers and Language Processing: Flex and Bison ★★☆☆☆

### Competencies

* Creative Software Designer
* Clean Coder & Minimalist
* Excellent Troubleshooter
* Excellent Mentor
* Self-Learner
* Technical Writer
