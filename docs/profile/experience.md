# :fontawesome-solid-business-time: Working Experience

**Associate Software Architect<br/>**
Jun 2024 to Present<br/>
CodeGen International Pvt. Ltd.

`Data Engineering`, `Data Lakes`
<hr/>

**Senior Technical Team Lead<br/>**
Jul 2023 to May 2024<br/>
CodeGen International Pvt. Ltd.

`Python`, `LangChain`, `LLMs`, `OpenAI Function Calling`, `Java Instrumentation`
<hr/>

**Technical Team Lead<br/>**
Jan 2022 to May 2023<br/>
CodeGen International Pvt. Ltd.

I led the API team by manging sprint tasks, supervising internal solution developments, code reviewing, conducting training sessions
and, contributing for company decision makings.

`API Changelog`, `Agile Scrum`, `Jira`, `Vulnerability Scanning`
<hr/>

**Associate Technical Lead<br/>**
Mar 2021 to Dec 2021<br/>
CodeGen International Pvt. Ltd.

During this period, I joined into a new team that governs the APIs of internal projects. In there, I contributed by defining API best practices for REST APIs,
reviewing solutions and codes, refactoring API security framework, fixing vulnerabilities of API services, and introducing API documentation platform.
Also, I was able to participate in developer recruitment process as well.

`Open API 3`, `Identity Brokering`, `OAuth2`, `GitLab CI/CD`
<hr/>

**Senior Software Engineer<br/>**
Apr 2019 to Feb 2021<br/>
CodeGen International Pvt. Ltd.

In this position, I contributed to the same project by solutioning new features, documenting exiting features, developing internal tools and grooming junior developers.

`Spring`, `JPA`, `RESTful APIs` 
<hr/>

**Software Engineer<br/>**
Jul 2017 to Mar 2019<br/>
CodeGen International Pvt. Ltd.

I joined CodeGen as a software engineer just after the university. I worked on a project that develops, a travel industry based software product.
It was a client project. In there, I contributed in new feature developments, enhancements and bug fixes.

`Java`, `Swing`, `SOAP Web Services`, `Jenkins`
<hr/>

**Trainee Engineer Technology<br/>**
March 2015 to September 2015<br/>
Virtusa Pvt. Ltd.

I joined Virtusa for a six-month internship as an undergraduate. During this period, I worked on a project that develops a static code analysis tool for T-SQL. 
The tool is mainly developed in Java language and the Antlr grammar files are used for parsing T-SQL scripts and generating Abstract Syntax Tree. 
I was able to practically use some theories that I learned in Automata Theory module in the university.

`Java`, `Maven`, `Antlr`
<hr/>