# :material-music: Extras

* Tutoring on Youtube for Spring technology
* Drawing and Sculpting
* Classical music and Sci-Fi movies

<iframe width="100%" height="450" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/353291681&color=%23444444&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe><div style="font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;"><a href="https://soundcloud.com/amila-chamikara-793828204" title="Amila Chamikara" target="_blank" style="color: #cccccc; text-decoration: none;">Amila Chamikara</a> · <a href="https://soundcloud.com/amila-chamikara-793828204/sets/amilas-audio-compositions" title="Amila&#x27;s Audio Covers" target="_blank" style="color: #cccccc; text-decoration: none;">Amila&#x27;s Audio Covers</a></div>