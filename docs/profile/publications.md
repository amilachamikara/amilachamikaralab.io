# :material-bookshelf: Publications

**Research Paper**: _"Sketch based database querying system"_, 2017 IEEE International
Conference on Industrial and Information Systems (ICIIS), Peradeniya.
[https://ieeexplore.ieee.org/document/8300391](https://ieeexplore.ieee.org/document/8300391){target=_blank}