# :material-school: Education

**BSc. (Hons.) in Information Technology**<br/>
Faculty of Information Technology,<br/>
University of Moratuwa, Sri Lanka.<br/>
2013-2017
<hr/>

**Biology, Chemistry and Physics**<br/>
Pinnawala National School,<br/>
Rambukkana, Sri Lanka.<br/>
2001-2010
<hr/>

**Primary Education**<br/>
Rajagiriya MMV, Dippitiya, Sri Lanka.<br/>
1996-2000
<hr/>